import XCTest
import Alamofire

@testable import Fruity

class OperationStatsTests: XCTestCase {

    func test_stats_networkLoadTime_method_isEqualToGet() {
        let method = Route.Stats.networkLoadTime(milliseconds: 0).method
        XCTAssertEqual(method, .get)
    }

    func test_stats_networkLoadTime_path_isEqualToStats() {
        let path = Route.Stats.networkLoadTime(milliseconds: 0).path
        XCTAssertEqual(path, "stats")
    }

    func test_stats_networkLoadTime_event_isEqualToLoad() {
        let event = Route.Stats.networkLoadTime(milliseconds: 0).event
        XCTAssertEqual(event, .load)
    }

    func test_stats_networkLoadTime_parameteres_isEqualToExpected() {
        let parameteres = Route.Stats.networkLoadTime(milliseconds: 100).parameteres

        guard let event = parameteres?["event"] as? String,
              let data = parameteres?["data"] as? Int
        else { return XCTFail("failed") }
        XCTAssertEqual(event, "load")
        XCTAssertEqual(data, 100)
    }

    func test_stats_pageDisplayTime_method_isEqualToGet() {
        let method = Route.Stats.pageDisplayTime(milliseconds: 0).method
        XCTAssertEqual(method, .get)
    }

    func test_stats_pageDisplayTime_path_isEqualToStats() {
        let path = Route.Stats.pageDisplayTime(milliseconds: 0).path
        XCTAssertEqual(path, "stats")
    }

    func test_stats_pageDisplayTime_event_isEqualToLoad() {
        let event = Route.Stats.pageDisplayTime(milliseconds: 0).event
        XCTAssertEqual(event, .display)
    }

    func test_stats_pageDisplayTime_parameteres_isEqualToExpected() {
        let parameteres = Route.Stats.pageDisplayTime(milliseconds: 200).parameteres

        guard let event = parameteres?["event"] as? String,
            let data = parameteres?["data"] as? Int
            else { return XCTFail("failed") }
        XCTAssertEqual(event, "display")
        XCTAssertEqual(data, 200)
    }

    func test_stats_exceptionRaisedOn_method_isEqualToGet() {
        let method = Route.Stats.exceptionRaisedOn(className: "test", line: 1).method
        XCTAssertEqual(method, .get)
    }

    func test_stats_exceptionRaisedOn_path_isEqualToStats() {
        let path = Route.Stats.exceptionRaisedOn(className: "test", line: 1).path
        XCTAssertEqual(path, "stats")
    }

    func test_stats_exceptionRaisedOn_event_isEqualToLoad() {
        let event = Route.Stats.exceptionRaisedOn(className: "test", line: 1).event
        XCTAssertEqual(event, .error)
    }

    func test_stats_exceptionRaisedOn_parameteres_isEqualToExpected() {
        let parameteres = Route.Stats.exceptionRaisedOn(className: "test", line: 1).parameteres

        guard let event = parameteres?["event"] as? String,
            let data = parameteres?["data"] as? String
            else { return XCTFail("failed") }
        XCTAssertEqual(event, "error")
        XCTAssertEqual(data, "Class test, Line 1")
    }

}
