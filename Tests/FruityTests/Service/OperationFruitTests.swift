import XCTest
import Alamofire

@testable import Fruity

class OperationFruitTests: XCTestCase {

    func test_fruit_all_methodEqualToGet() {
        let method = Route.Fruit.all.method
        XCTAssertEqual(method, .get)
    }

    func test_fruit_all_path_isEqualToDataJSON() {
        let path = Route.Fruit.all.path
        XCTAssertEqual(path, "data.json")
    }

    func test_fruit_all_parameters_isNil() {
        let parameteres = Route.Fruit.all.parameteres
        XCTAssertNil(parameteres)
    }

}
