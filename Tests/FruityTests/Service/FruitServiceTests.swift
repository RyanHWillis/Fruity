import XCTest

@testable import Fruity

class FruitServiceTests: XCTestCase {

    private var subject: FruitService!
    private var mockServiceClient: MockServiceClient!

    override func setUp() {
        super.setUp()
        guard let dictionary = try? JSONLoader.dictionaryFromResource(withName: "fruit_success")
            else { return }
        mockServiceClient = MockServiceClient()
        let stubbedServiceResponse = ServiceResponse(dictionary: dictionary, error: nil)
        mockServiceClient.stubbedServiceResponse = stubbedServiceResponse
        subject = FruitService(serviceClient: mockServiceClient)
    }

    override func tearDown() {
        subject = nil
        mockServiceClient = nil
        super.tearDown()
    }

    func test_getFruit_displayableFruit_isNotNil() {
        subject.getFruit { displayableFruit in
            XCTAssertNotNil(displayableFruit)
        }
    }

    func test_getFruit_numberOfDisplayableFruit_isEqualToNine() {
        subject.getFruit { displayableFruit in
            XCTAssertEqual(displayableFruit?.count, 9)
        }
    }

    func test_getFruit_correctOrder_firstDisplayableFruitEqualsExpected() {
        let fruitType = FruitType(stringLiteral: "apple")
        let fruitWeight = FruitWeight(inGrams: 120)
        let fruit = Fruit(type: fruitType, price: 149, weight: fruitWeight)
        let expectedDisplayableFruit = DisplayableFruit(fruit: fruit)
        subject.getFruit { displayableFruit in
            XCTAssertEqual(displayableFruit?.first, expectedDisplayableFruit)
        }
    }

}
