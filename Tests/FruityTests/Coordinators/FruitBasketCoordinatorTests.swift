import XCTest

@testable import Fruity

class FruitBasketCoordinatorTests: XCTestCase {

    private var subject: FruitBasketCoordinator!
    private var navigationController: UINavigationController!

    override func setUp() {
        super.setUp()
        navigationController = UINavigationController()
        subject = FruitBasketCoordinator(presenter: navigationController,
                                         animated: false)
    }

    override func tearDown() {
        subject = nil
        navigationController = nil
        super.tearDown()
    }

    func test_start_navigtionViewControllersCount_isEqualToOne() {
        subject.start()
        XCTAssertEqual(navigationController.viewControllers.count, 1)
    }

    func test_start_pushedViewControllerType_isFruitBasketViewController() {
        subject.start()
        XCTAssertTrue(navigationController.viewControllers.first is FruitBasketViewController)
    }

    func test_viewControllerDidSelect_increasesViewControllerCount_equalToTwo() {
        subject.start()
        let fruitType = FruitType(stringLiteral: "type")
        let fruitWeight = FruitWeight(inGrams: 20)
        let fruit = Fruit(type: fruitType, price: 10, weight: fruitWeight)
        let displayableFruit = DisplayableFruit(fruit: fruit)
        subject.viewControllerDidSelect(displayableFruit: displayableFruit)
        XCTAssertEqual(navigationController.viewControllers.count, 2)
    }

    func test_viewControllerDidSelect_pushedViewController_isFruitDetailViewController() {
        subject.start()
        let fruitType = FruitType(stringLiteral: "type")
        let fruitWeight = FruitWeight(inGrams: 20)
        let fruit = Fruit(type: fruitType, price: 10, weight: fruitWeight)
        let displayableFruit = DisplayableFruit(fruit: fruit)
        subject.viewControllerDidSelect(displayableFruit: displayableFruit)
        XCTAssertTrue(navigationController.viewControllers.last is FruitDetailViewController)
    }

    func test_viiewControllerDidSelect_pushedViewController_hasEqualDisplayableFruit() {
        subject.start()
        let fruitType = FruitType(stringLiteral: "type")
        let fruitWeight = FruitWeight(inGrams: 20)
        let fruit = Fruit(type: fruitType, price: 10, weight: fruitWeight)
        let displayableFruit = DisplayableFruit(fruit: fruit)
        subject.viewControllerDidSelect(displayableFruit: displayableFruit)
        let viewController = navigationController.viewControllers.last as? FruitDetailViewController
        XCTAssertEqual(viewController?.viewModel.displayableFruit, displayableFruit)
    }

}
