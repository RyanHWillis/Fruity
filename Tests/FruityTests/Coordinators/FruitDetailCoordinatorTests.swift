import XCTest

@testable import Fruity

class FruitDetailCoordinatorTests: XCTestCase {

    private var subject: FruitDetailCoordinator!
    private var navigationController: UINavigationController!
    private var displayableFruit: DisplayableFruit!

    override func setUp() {
        super.setUp()

        let fruitType = FruitType(stringLiteral: "type")
        let fruitWeight = FruitWeight(inGrams: 20)
        let fruit = Fruit(type: fruitType, price: 10, weight: fruitWeight)
        displayableFruit = DisplayableFruit(fruit: fruit)

        navigationController = UINavigationController()
        subject = FruitDetailCoordinator(presenter: navigationController,
                                         displayableFruit: displayableFruit,
                                         animated: false)
    }

    override func tearDown() {
        subject = nil
        navigationController = nil
        super.tearDown()
    }

    func test_start_navigtionViewControllersCount_isEqualToOne() {
        subject.start()
        XCTAssertEqual(navigationController.viewControllers.count, 1)
    }

    func test_start_pushedViewControllerType_isFruitDetailViewController() {
        subject.start()
        XCTAssertTrue(navigationController.viewControllers.first is FruitDetailViewController)
    }

    func test_start_initDisplayableFruit_equalToInitFruitDetailDisplaybleFruit() {
        subject.start()
        let viewController = navigationController.viewControllers.last as? FruitDetailViewController
        let viewModel = viewController?.viewModel
        XCTAssertEqual(viewModel?.displayableFruit, displayableFruit)
    }

}
