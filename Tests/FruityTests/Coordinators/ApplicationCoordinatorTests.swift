import XCTest

@testable import Fruity

class ApplicationCoordinatorTests: XCTestCase {

    private var subject: ApplicationCoordinator!
    private var rootViewControler: UINavigationController!

    override func setUp() {
        super.setUp()
        let window = UIWindow(frame: UIScreen.main.bounds)
        rootViewControler = UINavigationController()
        subject = ApplicationCoordinator(window: window,
                                         rootViewController: rootViewControler,
                                         animated: false)

    }

    override func tearDown() {
        subject = nil
        rootViewControler = nil
        super.tearDown()
    }

    func test_start_controllerIsFruitBasketViewController() {
        subject.start()
        XCTAssertEqual(rootViewControler.viewControllers.count, 1)
    }

    func test_start_viewControllerType_isFruitBasketViewController() {
        subject.start()
        XCTAssertTrue(rootViewControler.viewControllers.last is FruitBasketViewController)
    }

}
