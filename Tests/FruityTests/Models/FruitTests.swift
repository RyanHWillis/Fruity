import XCTest

@testable import Fruity

class FruitTests: XCTestCase {

    private var subject: Fruit!
    private var fruitType: FruitType!
    private var fruitWeight: FruitWeight!

    override func setUp() {
        super.setUp()
        fruitType = FruitType(stringLiteral: "type")
        fruitWeight = FruitWeight(inGrams: 20)
        subject = Fruit(type: fruitType, price: 10, weight: fruitWeight)
    }

    override func tearDown() {
        subject = nil
        fruitType = nil
        fruitType = nil
        super.tearDown()
    }

    func test_init_type_isEqualToValueType() {
        XCTAssertEqual(subject.type.rawValue, "type")
    }

    func test_init_price_isEqualToValueTen() {
        XCTAssertEqual(subject.price, 10)
    }

    func test_init_weight_isEqualToTwenty() {
        XCTAssertEqual(subject.weight.rawValue, 20)
    }

    func test_init_allValues_isNotNil() {
        XCTAssertNotNil(subject)
    }

    func test_init_typeNil_subjectIsEqualToNil() {
        let subject = Fruit(type: nil, price: 10, weight: fruitWeight)
        XCTAssertNil(subject)
    }

    func test_init_priceNil_subjectIsEqualToNil() {
        let subject = Fruit(type: fruitType, price: nil, weight: fruitWeight)
        XCTAssertNil(subject)
    }

    func test_init_weightNil_subjectIsEqualToNil() {
        let subject = Fruit(type: fruitType, price: 10, weight: nil)
        XCTAssertNil(subject)
    }

    func test_init_validDictionary_subjectIsNotEqualToNil() {
        let dictionary: [String: Any] = ["type": "type",
                                         "price": 10,
                                         "weight": Float(20.5)]
        let subject = Fruit(dictionary: dictionary)
        XCTAssertNotNil(subject)
    }

    func test_init_dictionaryWithoutWeight_subjectIsNil() {
        let dictionary: [String: Any] = ["type": "type",
                                         "price": 10,
                                         "what": 20]
        let subject = Fruit(dictionary: dictionary)
        XCTAssertNil(subject)
    }

    func test_init_dictionaryWithoutPrice_subjectIsNil() {
        let dictionary: [String: Any] = ["type": "type",
                                         "priice": 10,
                                         "weight": 20]
        let subject = Fruit(dictionary: dictionary)
        XCTAssertNil(subject)
    }

    func test_init_dictionaryWithoutType_subjectIsNil() {
        let dictionary: [String: Any] = ["types": "type",
                                         "price": 10,
                                         "weight": 20]
        let subject = Fruit(dictionary: dictionary)
        XCTAssertNil(subject)
    }

}
