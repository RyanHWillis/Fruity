import XCTest

@testable import Fruity

class DisplayableFruitTests: XCTestCase {

    private var subject: DisplayableFruit!

    override func setUp() {
        super.setUp()
        let fruitType = FruitType(stringLiteral: "type")
        let fruitWeight = FruitWeight(inGrams: 20)
        let fruit = Fruit(type: fruitType, price: 10, weight: fruitWeight)
        subject = DisplayableFruit(fruit: fruit)
    }

    override func tearDown() {
        subject = nil
        super.tearDown()
    }

    func test_equatable_modelWithEqualValues_isEqual() {
        XCTAssertEqual(subject, subject)
    }

    func test_init_type_isEqualToValueType() {
        XCTAssertEqual(subject.type, "Yummy type")
    }

    func test_init_price_isEqualToValueTen() {
        XCTAssertEqual(subject.price, "£10.00")
    }

    func test_init_weight_isEqualToValueTwenty() {
        XCTAssertEqual(subject.weight, "0.02kg")
    }

}
