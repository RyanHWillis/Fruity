import Foundation

@testable import Fruity

class MockServiceClient: NSObject,
                         ServicableClientProtocol {

    var stubbedServiceResponse: ServiceResponse!

    func callService(operation: ServiceOperation,
                     completion: @escaping (ServiceResponse) -> Void) {
        completion(stubbedServiceResponse)
    }

}
