import UIKit

class MockUICollectionView: UICollectionView {

    var stubbedCollectionViewCell = UICollectionViewCell()

    override func dequeueReusableCell(withReuseIdentifier identifier: String,
                                      for indexPath: IndexPath) -> UICollectionViewCell {
        return stubbedCollectionViewCell
    }

}
