import Foundation

@testable import Fruity

class MockFruitService: FruitService {

    var stubbedDisplayableFruit: [DisplayableFruit]?

    override func getFruit(completion: @escaping ([DisplayableFruit]?) -> Void) {
        completion(stubbedDisplayableFruit)
    }

}
