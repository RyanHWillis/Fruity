import XCTest

@testable import Fruity

class NSNumberCurrencyTests: XCTestCase {

    func test_currencyFormatted_wholeNumberTwoFractionDigits_isEqualToExpectedFormat() {
        let number = NSNumber(value: 10.00)
        let numberCurrencyFormatted = number.currencyFormattedGBP()
        XCTAssertEqual(numberCurrencyFormatted, "£10.00")
    }

    func test_currencyFormatted_fractionNumber_isEqualToExpectedFormat() {
        let number = NSNumber(value: 10.50)
        let numberCurrencyFormatted = number.currencyFormattedGBP()
        XCTAssertEqual(numberCurrencyFormatted, "£10.50")
    }

    func test_currencyFormatted_oneNumberAfterDecimal_isEqualToExpectedFormat() {
        let number = NSNumber(value: 10.5)
        let numberCurrencyFormatted = number.currencyFormattedGBP()
        XCTAssertEqual(numberCurrencyFormatted, "£10.50")
    }

    func test_currencyFormatted_wholeNumberNoDecimal_isEqualToExpectedFormat() {
        let number = NSNumber(value: 10)
        let numberCurrencyFormatted = number.currencyFormattedGBP()
        XCTAssertEqual(numberCurrencyFormatted, "£10.00")
    }

}
