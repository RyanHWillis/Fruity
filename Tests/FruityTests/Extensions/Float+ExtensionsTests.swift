import XCTest

@testable import Fruity

class FloatExtensionsTests: XCTestCase {

    func test_dividedByOneThousand_valueEqualsOne() {
        let float: Float = 1000.0
        XCTAssertEqual(float.dividedByOneThousand, 1)
    }

}
