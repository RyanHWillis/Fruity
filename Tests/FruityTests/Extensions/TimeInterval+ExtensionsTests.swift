import XCTest

@testable import Fruity

class TimeIntervalExtensionsTests: XCTestCase {

    func test_milliseconds_wholeNumber_isEqualToExpectedValue() {
        let timeInterval: TimeInterval = 1
        XCTAssertEqual(timeInterval.milliseconds, 1000)
    }

    func test_milliseconds_oneDecimalNumber_isEqualToExpectedValue() {
        let timeInterval: TimeInterval = 50.5
        XCTAssertEqual(timeInterval.milliseconds, 50500)
    }

    func test_milliseconds_twoDecimalNumber_isEqualToExpectedValue() {
        let timeInterval: TimeInterval = 100.50
        XCTAssertEqual(timeInterval.milliseconds, 100500)
    }

}
