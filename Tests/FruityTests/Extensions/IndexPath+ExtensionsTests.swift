import XCTest

@testable import Fruity

class IndexPathExtensionsTests: XCTestCase {

    func test_zero_indexPath_isEqualToFirstIndex() {
        let indexPath = IndexPath(row: 0,
                                  section: 0)
        XCTAssertEqual(IndexPath.zero, indexPath)
    }

}
