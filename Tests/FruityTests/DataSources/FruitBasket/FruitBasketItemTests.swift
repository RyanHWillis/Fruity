import XCTest

@testable import Fruity

class FruitBasketItemTests: XCTestCase {

    private var subject: FruitBasketItem!
    private var mockCollectionView: MockUICollectionView!

    override func setUp() {
        super.setUp()

        let fruitType = FruitType(stringLiteral: "type")
        let fruitWeight = FruitWeight(inGrams: 20)
        let fruit = Fruit(type: fruitType, price: 10, weight: fruitWeight)
        let displayableFruit = DisplayableFruit(fruit: fruit)
        subject = FruitBasketItem(displayableFruit: displayableFruit,
                                  didSelectFruit: { _ in })
        let rect = CGRect(x: 0, y: 0, width: 200, height: 200)
        mockCollectionView = MockUICollectionView(frame: rect,
                                                  collectionViewLayout: UICollectionViewLayout())
        mockCollectionView.registerNib(FruitBasketItemCell.self)
        mockCollectionView.stubbedCollectionViewCell = FruitBasketItemCell.fromNib()
    }

    override func tearDown() {
        subject = nil
        super.tearDown()
    }

    func test_cell_type_isFruitBasketItemCell() {
        let cell = subject.cell(collectionView: mockCollectionView,
                                indexPath: .zero)
        XCTAssertTrue(cell is FruitBasketItemCell)
    }

    func test_cell_type_initIsEqualToCellType() {
        let cell = subject.cell(collectionView: mockCollectionView,
                                indexPath: .zero) as? FruitBasketItemCell
        XCTAssertEqual(subject.displayableFruit.type, cell!.type)
    }

    func test_size_width_isEqualToCollectionViewBoundsWith() {
        let size = subject.size(collectionView: mockCollectionView,
                                indexPath: .zero)
        XCTAssertEqual(size.width, 200)
    }

    func test_size_height_isEqualToCollectionViewBoundsHeight() {
        let size = subject.size(collectionView: mockCollectionView,
                                indexPath: .zero)
        XCTAssertEqual(size.height, 100)
    }

    func test_shouldSelect_isEqualToFalse() {
        XCTAssertTrue(subject.shouldSelect)
    }

    func test_didSelect_isEqualToNil() {
        XCTAssertNotNil(subject.didSelect)
    }

}
