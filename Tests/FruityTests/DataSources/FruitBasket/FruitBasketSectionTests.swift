import XCTest

@testable import Fruity

class FruitBasketSectionTests: XCTestCase {

    private var subject: FruitBasketSection!
    private var displayableFruitOne: DisplayableFruit!
    private var displayableFruitTwo: DisplayableFruit!

    override func setUp() {
        super.setUp()

        let fruitTypeOne = FruitType(stringLiteral: "banana")
        let fruitWeightOne = FruitWeight(inGrams: 30)
        let fruitOne = Fruit(type: fruitTypeOne, price: 10, weight: fruitWeightOne)
        displayableFruitOne = DisplayableFruit(fruit: fruitOne)

        let fruitTypeTwo = FruitType(stringLiteral: "apple")
        let fruitWeightTwo = FruitWeight(inGrams: 40)
        let fruitTwo = Fruit(type: fruitTypeTwo, price: 20, weight: fruitWeightTwo)
        displayableFruitTwo = DisplayableFruit(fruit: fruitTwo)

        subject = FruitBasketSection(displayableFruit: [displayableFruitOne, displayableFruitTwo],
                                     didSelectFruit: { _ in })
    }

    override func tearDown() {
        subject = nil
        displayableFruitOne = nil
        displayableFruitTwo = nil
        super.tearDown()
    }

    func test_items_type_isFruitBasketItem() {
        let items = subject.items
        XCTAssertTrue(items is [FruitBasketItem])
    }

    func test_items_count_isEqualToInitDisplayableFruit() {
        XCTAssertEqual(subject.items.count, 2)
    }

    func test_items_first_isEqualToFirstInitDisplayableFruit() {
        let item = subject.items.first as? FruitBasketItem
        XCTAssertEqual(item?.displayableFruit, displayableFruitOne)
    }

    func test_items_last_isEqualToLastInitDisplayableFruit() {
        let item = subject.items.last as? FruitBasketItem
        XCTAssertEqual(item?.displayableFruit, displayableFruitTwo)
    }

}
