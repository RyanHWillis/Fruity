import XCTest

@testable import Fruity

class FruitBasketViewModelTests: XCTestCase {

    private var subject: FruitBasketViewModel!
    private var mockFruitService: MockFruitService!

    override func setUp() {
        super.setUp()
        mockFruitService = MockFruitService()

        let fruitTypeOne = FruitType(stringLiteral: "banana")
        let fruitWeightOne = FruitWeight(inGrams: 30)
        let fruitOne = Fruit(type: fruitTypeOne, price: 10, weight: fruitWeightOne)
        let displayableFruitOne = DisplayableFruit(fruit: fruitOne)

        let fruitTypeTwo = FruitType(stringLiteral: "apple")
        let fruitWeightTwo = FruitWeight(inGrams: 40)
        let fruitTwo = Fruit(type: fruitTypeTwo, price: 20, weight: fruitWeightTwo)
        let displayableFruitTwo = DisplayableFruit(fruit: fruitTwo)

        mockFruitService.stubbedDisplayableFruit = [displayableFruitOne,
                                                    displayableFruitTwo]

        subject = FruitBasketViewModel(service: mockFruitService)
    }

    override func tearDown() {
        subject = nil
        mockFruitService = nil
        super.tearDown()
    }

    func test_getFruit_displayableFruit_countEqualToTwo() {
        subject.getFruit { (displayableFruit) in
            XCTAssertEqual(displayableFruit?.count, 2)
        }
    }

    func test_getFruit_firstDisplayableFruitType_isEqualToBanana() {
        subject.getFruit { displayableFruit in
            XCTAssertEqual(displayableFruit?.first?.type, "Yummy banana")
        }
    }

    func test_getFruit_secondDisplayableFruitType_isEqualToApple() {
        subject.getFruit { displayableFruit in
            XCTAssertEqual(displayableFruit?.last?.type, "Yummy apple")
        }
    }

}
