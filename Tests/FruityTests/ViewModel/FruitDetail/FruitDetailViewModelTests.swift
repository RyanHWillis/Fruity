import XCTest

@testable import Fruity

class FruitDetailViewModelTests: XCTestCase {

    private var subject: FruitDetailViewModel!

    override func setUp() {
        super.setUp()
        let fruitType = FruitType(stringLiteral: "type")
        let fruitWeight = FruitWeight(inGrams: 20)
        let fruit = Fruit(type: fruitType, price: 10, weight: fruitWeight)
        let displayableFruit = DisplayableFruit(fruit: fruit)
        subject = FruitDetailViewModel(displayableFruit: displayableFruit)
    }

    override func tearDown() {
        subject = nil
        super.tearDown()
    }

    func test_init_type_isEqualToValueType() {
        XCTAssertEqual(subject.type, "Yummy type")
    }

    func test_init_price_isEqualToValuePrice() {
        XCTAssertEqual(subject.price, "£10.00")
    }

    func test_init_weight_isEqualToValueWeight() {
        XCTAssertEqual(subject.weight, "0.02kg")
    }

}
