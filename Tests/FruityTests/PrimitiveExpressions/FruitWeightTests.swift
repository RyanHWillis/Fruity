import XCTest

@testable import Fruity

class FruitWeightTests: XCTestCase {

    private var subject: FruitWeight!

    override func setUp() {
        super.setUp()
        subject = FruitWeight(inGrams: 1000)
    }

    override func tearDown() {
        subject = nil
        super.tearDown()
    }

    func test_rawValue_roundFloat_isEqualToInitGrams() {
        XCTAssertEqual(subject.rawValue, 1000)
    }

    func test_gramsToKiliograms_initRoundGrams_isEqualToOne() {
        XCTAssertEqual(subject.gramsToKillograms, 1)
    }

    func test_gramsToKiliograms_initDecimalGrams_isEqualToOne() {
        subject = FruitWeight(inGrams: 1000.50)
        XCTAssertEqual(subject.gramsToKillograms, 1.0005)
    }

}
