import XCTest

@testable import Fruity

class FruitTypeTests: XCTestCase {

    private var subject: FruitType!

    override func setUp() {
        super.setUp()
        subject = FruitType(type: "banana")
    }

    override func tearDown() {
        subject = nil
        super.tearDown()
    }

    func test_rawValue_type_isEqualToBanana() {
        XCTAssertEqual(subject.rawValue, "banana")
    }

}
