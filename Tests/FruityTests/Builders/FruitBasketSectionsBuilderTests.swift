import XCTest

@testable import Fruity

class FruitBasketSectionsBuilderTests: XCTestCase {

    private var subject: FruitBasketSectionsBuilder!
    private var displayableFruit: [DisplayableFruit]!

    override func setUp() {
        super.setUp()
        let fruitType = FruitType(stringLiteral: "type")
        let fruitWeight = FruitWeight(inGrams: 20)
        let fruit = Fruit(type: fruitType, price: 10, weight: fruitWeight)
        displayableFruit = [DisplayableFruit(fruit: fruit)]
        subject = FruitBasketSectionsBuilder(displayableFruit: displayableFruit,
                                             didSelectFruit: { _ in })
    }

    override func tearDown() {
        subject = nil
        displayableFruit = nil
        super.tearDown()
    }

    func test_build_numberOfCollectionViewSections_isEqualToOne() {
        let sections = subject.build()
        XCTAssertEqual(sections.count, 1)
    }

    func test_build_section_isFruitBasketSection() {
        let sections = subject.build()
        XCTAssertTrue(sections.first is FruitBasketSection)
    }

    func test_build_sections_section_equalToInitDisplayableFruitCount() {
        let sections = subject.build()
        let section = sections.first as? FruitBasketSection
        XCTAssertEqual(section?.displayableFruit, displayableFruit)
    }

}
