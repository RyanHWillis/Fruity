import Foundation

extension Float {

    var dividedByOneThousand: Float {
        return self / 1000
    }

}
