import Foundation

extension TimeInterval {

    var milliseconds: Int {
        return Int(self * 1000)
    }

}
