import UIKit

extension IndexPath {

    static var zero: IndexPath {
        return IndexPath(row: 0,
                         section: 0)
    }

}
