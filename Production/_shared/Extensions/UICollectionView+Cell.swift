import UIKit

extension UICollectionView {

    func registerNib<T: UICollectionViewCell>(_ nibClass: T.Type) {
        let nib = UINib(nibName: String(describing: T.self),
                        bundle: Bundle(for: nibClass))
        register(nib, forCellWithReuseIdentifier: String(describing: nibClass))
    }

    func registerHeader<T: UICollectionReusableView>(_ nibClass: T.Type) {
        registerNib(nibClass,
                    forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader)
    }

    func registerFooter<T: UICollectionReusableView>(_ nibClass: T.Type) {
        registerNib(nibClass,
                    forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter)
    }

    private func registerNib<T: UICollectionReusableView>(_ nibClass: T.Type,
                                                          forSupplementaryViewOfKind kind: String) {
        let nib = UINib(nibName: String(describing: T.self),
                        bundle: Bundle(for: T.self))
        register(nib,
                 forSupplementaryViewOfKind: kind,
                 withReuseIdentifier: String(describing: nibClass))
    }

    //swiftlint:disable force_cast
    func dequeue<T: UICollectionViewCell>(_ indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: String(describing: T.self),
                                   for: indexPath) as! T
    }

    func dequeueHeader<T: UICollectionReusableView>(for indexPath: IndexPath) -> T {
        return dequeue(kind: UICollectionView.elementKindSectionHeader,
                       for: indexPath)
    }

    func dequeueFooter<T: UICollectionReusableView>(for indexPath: IndexPath) -> T {
        return dequeue(kind: UICollectionView.elementKindSectionFooter,
                       for: indexPath)

    }

    //swiftlint:disable force_cast
    private func dequeue<T: UICollectionReusableView>(kind: String, for indexPath: IndexPath) -> T {
        return dequeueReusableSupplementaryView(ofKind: kind,
                                                withReuseIdentifier: String(describing: T.self),
                                                for: indexPath) as! T
    }

}
