import UIKit

//swiftlint:disable force_cast
extension UIView {

    class func fromNib<T: UIView>(nibName: String) -> T {
        return loadNib(nibName) as! T
    }

    class func fromNib() -> Self {
        func loadedView<T>() -> T where T: UIView {
            return loadNib(String(describing: T.self)) as! T
        }
        return loadedView()
    }

    private class func loadNib(_ named: String,
                               owner: UIViewController? = nil,
                               bundle: Bundle? = nil) -> UIView? {
        return UINib(nibName: named,
                     bundle: bundle)
            .instantiate(withOwner: owner,
                         options: nil).first as? UIView
    }

}
