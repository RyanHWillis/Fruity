import Foundation

class StatsErrorService: NSObject,
                         StatsErrorServicable {

    private let serviceStats: ServicableStatsProtocol

    init(serviceStats: ServicableStatsProtocol = StatsService()) {
        self.serviceStats = serviceStats
    }

    func trackErrorEvent(classType: AnyClass, line: Int) {
        let className = String(describing: classType)
        let operation = Route.Stats.exceptionRaisedOn(className: className, line: line)
        serviceStats.sendStats(operation: operation) { success in
            print(success)
        }
    }

}
