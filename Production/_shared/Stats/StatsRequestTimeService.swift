import Foundation

class StatsRequestTimeService: NSObject,
                               StatsLoadServicable {

    private let serviceStats: ServicableStatsProtocol

    init(serviceStats: ServicableStatsProtocol = StatsService()) {
        self.serviceStats = serviceStats
    }

    func trackNetworkLoad(time: TimeInterval) {
        let milliseconds = time.milliseconds
        let operation = Route.Stats.networkLoadTime(milliseconds: milliseconds)
        serviceStats.sendStats(operation: operation) { success in
            print("Stat Tracked: \(success). Network request took \(milliseconds) milliseconds")
        }
    }

}
