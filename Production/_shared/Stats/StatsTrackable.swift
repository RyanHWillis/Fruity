import Foundation

enum StatsEvent: String {
    case load
    case display
    case error
}

protocol StatsTrackable: StatsLoadServicable, StatsDisplayServicable, StatsErrorServicable {}

protocol StatsLoadServicable {
    func trackNetworkLoad(time: TimeInterval)
}

protocol StatsDisplayServicable {
    func trackDisplay(time: TimeInterval)
}

protocol StatsErrorServicable {
    func trackErrorEvent(classType: AnyClass, line: Int)
}
