import Foundation
import Alamofire

protocol ServicableStatsProtocol {
    func sendStats(operation: ServiceOperation,
                   completion: @escaping (Bool) -> Void)
}

class StatsService: NSObject,
                    ServicableStatsProtocol {

    func sendStats(operation: ServiceOperation, completion: @escaping (Bool) -> Void) {
        Alamofire.request(operation).responseJSON { (response: DataResponse<Any>) in
            completion(response.error == nil)
        }
    }

}
