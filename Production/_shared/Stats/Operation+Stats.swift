import Foundation
import Alamofire

extension Route {

    enum Stats: StatsOperation {

        case networkLoadTime(milliseconds: Int)
        case pageDisplayTime(milliseconds: Int)
        case exceptionRaisedOn(className: String, line: Int)

        var method: HTTPMethod {
            return .get
        }

        var path: String {
            return "stats"
        }

        var event: StatsEvent {
            switch self {
            case .networkLoadTime:
                return .load
            case .pageDisplayTime:
                return .display
            case .exceptionRaisedOn:
                return .error
            }
        }

        var parameteres: [String: Any]? {
            switch self {
            case .networkLoadTime(let milliseconds):
                return ["event": event.rawValue, "data": milliseconds]
            case .pageDisplayTime(let milliseconds):
                return ["event": event.rawValue, "data": milliseconds]
            case .exceptionRaisedOn(let className, let line):
                return ["event": event.rawValue, "data": "Class \(className), Line \(line)"]
            }
        }

        func asURLRequest() throws -> URLRequest {
            return try URLEncoding.default.encode(baseRequest(), with: parameteres)
        }

    }

}
