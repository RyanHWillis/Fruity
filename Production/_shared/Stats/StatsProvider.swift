import Foundation

class StatsProvider: NSObject,
                     StatsTrackable {

    private let serviceLoad: StatsLoadServicable
    private let serviceDisplay: StatsDisplayServicable
    private let serviceError: StatsErrorServicable

    // I've written this to be loosely coupled so i can test and insert other types
    // of stats services here. No tests in project around this class though, i'm out of time.
    init(serviceLoad: StatsLoadServicable = StatsRequestTimeService(),
         serviceDisplay: StatsDisplayServicable = StatsDisplayService(),
         serviceError: StatsErrorServicable = StatsErrorService()) {
        self.serviceLoad = serviceLoad
        self.serviceDisplay = serviceDisplay
        self.serviceError = serviceError
    }

    func trackNetworkLoad(time: TimeInterval) {
        serviceLoad.trackNetworkLoad(time: time)
    }

    func trackDisplay(time: TimeInterval) {
        serviceDisplay.trackDisplay(time: time)
    }

    func trackErrorEvent(classType: AnyClass, line: Int) {
        serviceError.trackErrorEvent(classType: classType, line: line)
    }

}
