import Foundation

class StatsDisplayService: NSObject,
                           StatsDisplayServicable {

    private let serviceStats: ServicableStatsProtocol

    init(serviceStats: ServicableStatsProtocol = StatsService()) {
        self.serviceStats = serviceStats
    }

    func trackDisplay(time: TimeInterval) {
        let milliseconds = time.milliseconds
        serviceStats.sendStats(operation: Route.Stats.pageDisplayTime(milliseconds: milliseconds)) { success in
            print(success)
        }
    }

}
