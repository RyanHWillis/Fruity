import UIKit

struct GenericCollectionViewSection: CollectionViewSection {

    let items: [CollectionViewItem]

    init(items: [CollectionViewItem]) {
        self.items = items
    }

}
