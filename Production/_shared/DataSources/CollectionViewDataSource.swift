import UIKit

class CollectionViewDataSource: NSObject,
                                UICollectionViewDataSource,
                                UICollectionViewDelegate {

    let collectionView: UICollectionView
    let sections: [CollectionViewSection]

    convenience init(collectionView: UICollectionView,
                     items: [CollectionViewItem]) {
        self.init(collectionView: collectionView,
                  sections: [GenericCollectionViewSection(items: items)])
    }

    init(collectionView: UICollectionView,
         sections: [CollectionViewSection]) {
        self.collectionView = collectionView
        self.sections = sections
        super.init()
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return sections[section].cellCount
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return sections[indexPath.section].cell(collectionView: collectionView,
                                                indexPath: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return sections[indexPath.section].shouldSelectItem(item: indexPath.item)
    }

    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        return sections[indexPath.section].didSelect(collectionView,
                                                     indexPath: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            return sections[indexPath.section].headerView
        case UICollectionView.elementKindSectionFooter:
            return sections[indexPath.section].footerView
        default:
            return UICollectionReusableView()
        }
    }

}
