import UIKit

protocol CollectionViewItem {

    var shouldSelect: Bool { get}
    var didSelect: (() -> Void)? { get }

    func cell(collectionView: UICollectionView,
              indexPath: IndexPath) -> UICollectionViewCell
    func size(collectionView: UICollectionView,
              indexPath: IndexPath) -> CGSize

}

extension CollectionViewItem {

    var didSelect: (() -> Void)? {
        return nil
    }

    var shouldSelect: Bool {
        return didSelect != nil
    }

    func size(collectionView: UICollectionView,
              indexPath: IndexPath) -> CGSize {
        return .zero
    }

}
