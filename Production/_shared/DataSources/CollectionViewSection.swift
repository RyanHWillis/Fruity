import UIKit

protocol CollectionViewSection {

    var items: [CollectionViewItem] { get }
    var cellCount: Int { get }
    var headerView: UICollectionReusableView { get }
    var headerSize: CGSize { get }
    var footerView: UICollectionReusableView { get }
    var footerSize: CGSize { get }

    func cell(collectionView: UICollectionView,
              indexPath: IndexPath) -> UICollectionViewCell
    func size(collectionView: UICollectionView,
              indexPath: IndexPath) -> CGSize
    func shouldSelectItem(item: Int) -> Bool
    func didSelect(_ collectionView: UICollectionView,
                   indexPath: IndexPath)

}

extension CollectionViewSection {

    var cellCount: Int {
        return items.count
    }

    var headerView: UICollectionReusableView {
        return UICollectionReusableView()
    }

    var headerSize: CGSize {
        return .zero
    }

    var footerView: UICollectionReusableView {
        return UICollectionReusableView()
    }

    var footerSize: CGSize {
        return .zero
    }

    func cell(collectionView: UICollectionView,
              indexPath: IndexPath) -> UICollectionViewCell {
        return items[indexPath.item].cell(collectionView: collectionView,
                                          indexPath: indexPath)
    }

    func size(collectionView: UICollectionView,
              indexPath: IndexPath) -> CGSize {
        return items[indexPath.item].size(collectionView: collectionView,
                                          indexPath: indexPath)
    }

    func shouldSelectItem(item: Int) -> Bool {
        return items[item].shouldSelect
    }

    func didSelect(_ collectionView: UICollectionView,
                   indexPath: IndexPath) {
        items[indexPath.item].didSelect?()
    }

}
