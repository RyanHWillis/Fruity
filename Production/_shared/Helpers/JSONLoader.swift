import Foundation

final class JSONLoader {

    static func dictionaryFromResource(withName name: String) throws -> [String: Any]? {
        do {
            guard let path = try self.path(forResource: name)
                else { return nil }
            let url = URL(fileURLWithPath: path)
            let data = try Data(contentsOf: url)
            let json = try? JSONSerialization.jsonObject(with: data,
                                                         options: [])
            let dictionary = json as? [String: Any]
            return dictionary
        } catch {
            return nil
        }
    }

    static private func path(forResource name: String) throws -> String? {
        let bundle = Bundle(for: JSONLoader.self)
        let path = bundle.path(forResource: name, ofType: "json")
        return path
    }

}
