import Foundation
import Alamofire

struct Route {
    static let baseUrlString = "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/"
}

protocol ServiceOperation: URLRequestConvertible {
    var method: HTTPMethod { get }
    var path: String { get }
    var parameteres: [String: Any]? { get }
    func baseRequest() throws -> URLRequest
}

protocol StatsOperation: ServiceOperation {
    var event: StatsEvent { get }
}

extension ServiceOperation {
    func baseRequest() throws -> URLRequest {
        let url = try Route.baseUrlString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        return urlRequest
    }
}
