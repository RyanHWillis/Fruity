import Foundation
import Alamofire

protocol OperationProtocol {
    associatedtype ReturnType
    var url: String { get }
    var parameters: [String: Any]? { get }
}

extension OperationProtocol {
    var parameters: [String: Any]? {
        return nil
    }
}

protocol ServicableClientProtocol {
    func callService(operation: ServiceOperation,
                     completion: @escaping (ServiceResponse) -> Void)
}

struct ServiceResponse {
    let dictionary: [String: Any]?
    let error: Error?
}
