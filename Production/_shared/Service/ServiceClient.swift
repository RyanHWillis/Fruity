import Foundation
import Alamofire

class ServiceClient: NSObject,
                     ServicableClientProtocol {

    private var statsNetworkLoadable: StatsLoadServicable

    init(statsNetworkLoadable: StatsLoadServicable = StatsRequestTimeService()) {
        self.statsNetworkLoadable = statsNetworkLoadable
    }

    func callService(operation: ServiceOperation,
                     completion: @escaping (ServiceResponse) -> Void) {

        Alamofire.request(operation).responseJSON { (response: DataResponse<Any>) in

            self.statsNetworkLoadable.trackNetworkLoad(time: response.timeline.requestDuration)

            guard let data = response.data
                else { return }
            let json = try? JSONSerialization.jsonObject(with: data,
                                                         options: [])
            let dictionary = json as? [String: Any]
            let serviceResponse = ServiceResponse(dictionary: dictionary,
                                                  error: response.error)
            completion(serviceResponse)
        }
    }

}
