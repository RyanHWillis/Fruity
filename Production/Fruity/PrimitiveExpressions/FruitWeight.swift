import Foundation

struct FruitWeight: ExpressibleByFloatLiteral {

    typealias FloatLiteralType = Float

    let rawValue: FloatLiteralType

    var gramsToKillograms: FloatLiteralType {
        return rawValue.dividedByOneThousand
    }

    init?(inGrams value: FruitWeight.FloatLiteralType?) {
        guard let value = value
            else { return nil }
        self.init(floatLiteral: value)
    }

    init(inGrams value: FruitWeight.FloatLiteralType) {
        self.init(floatLiteral: value)
    }

    init(floatLiteral value: FruitWeight.FloatLiteralType) {
        self.rawValue = value
    }

}
