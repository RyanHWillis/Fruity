import Foundation

struct FruitType: ExpressibleByStringLiteral {

    typealias StringLiteralType = String

    let rawValue: String

    init?(type value: FruitType.StringLiteralType?) {
        guard let value = value
            else { return nil }
        self.init(stringLiteral: value)
    }

    init(stringLiteral value: FruitType.StringLiteralType) {
        self.rawValue = value
    }

    init(extendedGraphemeClusterLiteral value: FruitType.StringLiteralType) {
        self.rawValue = value
    }

    init(unicodeScalarLiteral value: FruitType.StringLiteralType) {
        self.rawValue = value
    }

}
