import UIKit

class FruitBasketSectionsBuilder {

    private let displayableFruit: [DisplayableFruit]
    private let didSelectFruit: (DisplayableFruit) -> Void

    init(displayableFruit: [DisplayableFruit],
         didSelectFruit: @escaping (DisplayableFruit) -> Void) {
        self.displayableFruit = displayableFruit
        self.didSelectFruit = didSelectFruit
    }

    private lazy var sections: [CollectionViewSection] = {
        return [
            fruitSection
        ]
    }()

    private var fruitSection: CollectionViewSection {
        return FruitBasketSection(displayableFruit: displayableFruit,
                                  didSelectFruit: didSelectFruit)
    }

    func build() -> [CollectionViewSection] {
        return sections
    }

}
