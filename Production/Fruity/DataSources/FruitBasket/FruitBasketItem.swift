import UIKit

struct FruitBasketItem: CollectionViewItem {

    let displayableFruit: DisplayableFruit
    let didSelectFruit: (DisplayableFruit) -> Void

    func cell(collectionView: UICollectionView,
              indexPath: IndexPath) -> UICollectionViewCell {
        let cell: FruitBasketItemCell = collectionView.dequeue(indexPath)
        cell.type = displayableFruit.type
        return cell
    }

    func size(collectionView: UICollectionView,
              indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width,
                      height: 100)
    }

    var didSelect: (() -> Void)? {
        return {
            self.didSelectFruit(self.displayableFruit)
        }
    }

}
