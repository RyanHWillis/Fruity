import UIKit

struct FruitBasketSection: CollectionViewSection {

    let displayableFruit: [DisplayableFruit]
    let didSelectFruit: (DisplayableFruit) -> Void

    init(displayableFruit: [DisplayableFruit],
         didSelectFruit: @escaping (DisplayableFruit) -> Void) {
        self.displayableFruit = displayableFruit
        self.didSelectFruit = didSelectFruit
    }

    var items: [CollectionViewItem] {
        return displayableFruit.compactMap(item)
    }

    private func item(displayableFruit: DisplayableFruit) -> FruitBasketItem {
        return FruitBasketItem(displayableFruit: displayableFruit,
                               didSelectFruit: didSelectFruit)
    }

}
