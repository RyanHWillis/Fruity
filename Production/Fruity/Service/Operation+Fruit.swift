import UIKit

import Foundation
import Alamofire

extension Route {

    enum Fruit: ServiceOperation {

        case all

        var method: HTTPMethod {
            switch self {
            case .all:
                return .get
            }
        }

        var path: String {
            switch self {
            case .all:
                return "data.json"
            }
        }

        var parameteres: [String: Any]? {
            switch self {
            case .all:
                return nil
            }
        }

        func asURLRequest() throws -> URLRequest {
            var urlRequest = try self.baseRequest()
            switch self {
            case .all:
                urlRequest = try URLEncoding.default.encode(urlRequest, with: parameteres)
            }
            return urlRequest
        }
    }

}
