import Foundation
import Alamofire

class FruitService {

    private let serviceClient: ServicableClientProtocol

    init(serviceClient: ServicableClientProtocol = ServiceClient()) {
        self.serviceClient = serviceClient
    }

    func getFruit(completion: @escaping ([DisplayableFruit]?) -> Void) {
        serviceClient.callService(operation: Route.Fruit.all) { serviceResponse in
            let fruitList = serviceResponse.dictionary?[Key.fruit.rawValue] as? [[String: Any]]
            let fruit = fruitList?.compactMap(Fruit.init)
            let displayableFruit = fruit?.compactMap(DisplayableFruit.init)
            completion(displayableFruit)
        }
    }

}

extension FruitService {
    enum Key: String {
        case fruit
    }
}
