import UIKit

class FruitBasketViewController: UIViewController {

    @IBOutlet private weak var collectionView: UICollectionView!

    var viewModel: FruitBasketViewModel!
    weak var coordinator: FruitBasketCoordinatable!

    private let refreshControl = UIRefreshControl()
    private var dataSource: CollectionViewFlowLayoutDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "All the fruits"
        registerCells()
        addRefreshControl()
        getFruit()
    }

    @objc private func getFruit() {
        viewModel.getFruit { [weak self] displayableFruit in
            guard let displayableFruit = displayableFruit
                else { return }
            self?.loadDataSource(displayableFruit: displayableFruit)
            self?.refreshControl.endRefreshing()
        }
    }

    private func registerCells() {
        collectionView.registerNib(FruitBasketItemCell.self)
    }

    private func addRefreshControl() {
        refreshControl.addTarget(self, action: #selector(getFruit), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }

    private func loadDataSource(displayableFruit: [DisplayableFruit]) {
        let builder = FruitBasketSectionsBuilder(displayableFruit: displayableFruit,
                                                 didSelectFruit: didSelect)
        dataSource = CollectionViewFlowLayoutDataSource(collectionView: collectionView,
                                                        sections: builder.build())
    }

    private func didSelect(displayableFruit: DisplayableFruit) {
        coordinator.viewControllerDidSelect(displayableFruit: displayableFruit)
    }

}
