import UIKit

class FruitDetailViewController: UIViewController {

    @IBOutlet private weak var typeLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var weightLabel: UILabel!

    var viewModel: FruitDetailViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "those fruit details"
        setViewInformation()
    }

    private func setViewInformation() {
        typeLabel.text = viewModel.type
        priceLabel.text = viewModel.price
        weightLabel.text = viewModel.weight
    }

}
