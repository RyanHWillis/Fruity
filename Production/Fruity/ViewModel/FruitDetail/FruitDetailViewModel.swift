import Foundation

class FruitDetailViewModel: NSObject {

    let displayableFruit: DisplayableFruit

    init(displayableFruit: DisplayableFruit) {
        self.displayableFruit = displayableFruit
    }

    var type: String {
        return displayableFruit.type
    }

    var price: String {
        return displayableFruit.price
    }

    var weight: String {
        return displayableFruit.weight
    }

}
