import Foundation

class FruitBasketViewModel: NSObject {

    private let service: FruitService

    init(service: FruitService = FruitService()) {
        self.service = service
    }

    func getFruit(completion: @escaping ([DisplayableFruit]?) -> Void) {
        service.getFruit(completion: completion)
    }

}
