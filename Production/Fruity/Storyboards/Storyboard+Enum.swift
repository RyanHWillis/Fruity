import UIKit

enum Storyboard: String {

    case explore = "Explore"

    //swiftlint:disable force_cast
    func load<T: UIViewController>() -> T {
        let identifier = String(describing: T.self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! T
    }

    private var storyboard: UIStoryboard {
        return UIStoryboard(name: rawValue,
                            bundle: .main)
    }

}
