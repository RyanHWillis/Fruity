import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private lazy var applicationCoordinator: Coordinator = {
        self.makeCoordinator()
    }()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        applicationCoordinator.start()
        return true
    }

    private func makeCoordinator() -> Coordinator {
        let window = self.window ?? UIWindow(frame: UIScreen.main.bounds)
        return ApplicationCoordinator(window: window)
    }

}
