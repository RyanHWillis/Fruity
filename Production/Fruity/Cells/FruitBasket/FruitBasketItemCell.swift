import UIKit

class FruitBasketItemCell: UICollectionViewCell {

    @IBOutlet private weak var nameLabel: UILabel!

    var type: String? {
        didSet {
            nameLabel.text = type
        }
    }

}
