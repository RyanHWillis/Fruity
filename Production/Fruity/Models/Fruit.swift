import UIKit

struct Fruit {

    let type: FruitType
    let price: NSNumber
    let weight: FruitWeight

    init?(dictionary: [String: Any]) {
        let type = dictionary[Key.type.rawValue] as? String
        let price = dictionary[Key.price.rawValue] as? NSNumber
        let weight = dictionary[Key.weight.rawValue] as? Float

        self.init(type: FruitType(type: type),
                  price: price,
                  weight: FruitWeight(inGrams: weight))
    }

    init?(type: FruitType?,
          price: NSNumber?,
          weight: FruitWeight?) {

        guard let type = type,
              let price = price,
              let weight = weight
            else { return nil }

        self.init(type: type,
                  price: price,
                  weight: weight)
    }

    init(type: FruitType,
         price: NSNumber,
         weight: FruitWeight) {
        self.type = type
        self.price = price
        self.weight = weight
    }

}

extension Fruit {
    enum Key: String {
        case type
        case price
        case weight
    }
}
