import UIKit

struct DisplayableFruit: Equatable {

    private let fruit: Fruit

    init(fruit: Fruit) {
        self.fruit = fruit
    }

    var type: String {
        return "Yummy \(fruit.type.rawValue)"
    }

    var price: String {
        return fruit.price.currencyFormattedGBP() ?? "Price Unknown"
    }

    var weight: String {
        return "\(fruit.weight.gramsToKillograms)kg"
    }

    static func == (lhs: DisplayableFruit, rhs: DisplayableFruit) -> Bool {
        return lhs.type == rhs.type &&
               lhs.price == rhs.price &&
               lhs.weight == rhs.weight
    }

}
