import UIKit

class ApplicationCoordinator: Coordinator {

    private let window: UIWindow
    private let rootViewController: UINavigationController
    private let fruitBasketCoordinator: FruitBasketCoordinator
    private let animated: Bool

    init(window: UIWindow,
         rootViewController: UINavigationController = UINavigationController(),
         animated: Bool = true) {
        self.window = window
        self.rootViewController = rootViewController
        self.animated = animated
        fruitBasketCoordinator = FruitBasketCoordinator(presenter: rootViewController,
                                                        animated: animated)
    }

    func start() {
        window.rootViewController = rootViewController
        fruitBasketCoordinator.start()
        window.makeKeyAndVisible()
    }

}
