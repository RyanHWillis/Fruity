import UIKit

protocol FruitBasketCoordinatable: class {
    func viewControllerDidSelect(displayableFruit: DisplayableFruit)
}

class FruitBasketCoordinator: Coordinator,
                              FruitBasketCoordinatable {

    private let presenter: UINavigationController
    private let animated: Bool

    private var fruitBasketViewController: FruitBasketViewController?
    private var fruitDetailCoordinator: FruitDetailCoordinator?

    init(presenter: UINavigationController = UINavigationController(),
         animated: Bool = true) {
        self.presenter = presenter
        self.animated = animated
    }

    func start() {
        let viewController: FruitBasketViewController = Storyboard.explore.load()
        viewController.viewModel = FruitBasketViewModel()
        viewController.coordinator = self
        presenter.pushViewController(viewController, animated: animated)

        self.fruitBasketViewController = viewController
    }

    func viewControllerDidSelect(displayableFruit: DisplayableFruit) {
        fruitDetailCoordinator = FruitDetailCoordinator(presenter: presenter,
                                                        displayableFruit: displayableFruit,
                                                        animated: animated)
        fruitDetailCoordinator?.start()
    }

}
