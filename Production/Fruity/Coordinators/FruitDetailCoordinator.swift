import UIKit

class FruitDetailCoordinator: Coordinator {

    private let presenter: UINavigationController
    private let displayableFruit: DisplayableFruit
    private let animated: Bool

    private var fruitDetailCoordinator: FruitDetailViewController?

    init(presenter: UINavigationController,
         displayableFruit: DisplayableFruit,
         animated: Bool = true) {
        self.presenter = presenter
        self.displayableFruit = displayableFruit
        self.animated = animated
    }

    func start() {
        let viewController: FruitDetailViewController = Storyboard.explore.load()
        viewController.viewModel = FruitDetailViewModel(displayableFruit: displayableFruit)
        presenter.pushViewController(viewController, animated: animated)
        self.fruitDetailCoordinator = viewController
    }

}
